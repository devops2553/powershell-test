
$ErrorActionPreference= "silentlycontinue"

$update=$args[0]
$updateClient=$args[1]

if ( $update -eq "update" ) {
    Write-output "Updating..."

    $infoClient="{'text':'Starting updating condolivre-base - $updateClient'}"
    Invoke-RestMethod -Method post -ContentType 'Application/Json' -Body $infoClient -Uri https://livrefinanceira.webhook.office.com/webhookb2/356e4cab-c456-4560-9234-5b01b65bd872@b356ff48-3625-401d-8619-ccbdb542e5cd/IncomingWebhook/d20bc2c301134c66b27133a24f27c4e7/d064d489-5b81-4e07-8e7f-cbd1ca322884 | Out-Null

    #Stop process
    taskkill.exe /fi "imagename eq condolivre-base.exe" /f | Out-Null

    #Move bkp
    mv c:\condolivre\condolivre-base.exe c:\condolivre\bkp -Force | Out-Null

    sleep 2

    #Download new version
    wget https://gobase-condolivre-prod.s3.sa-east-1.amazonaws.com/condolivre-base.exe -O condolivre-base.exe | Out-Null

    sleep 2

    #Move new version
    mv .\condolivre-base.exe c:\condolivre -Force

    sleep 2

    #Start new version
    Write-Output "Starting condolivre-base.exe"
    Start-Process -FilePath c:\condolivre\condolivre-base.exe
    $counter = 0

    DO
    {
        Write-Output "Checking condolivre-base.exe..."
        $curlChecking=(Invoke-WebRequest -Uri "http://localhost:3000/health" -ContentType "application/json" -Method Get)
        $counter++

        #Timeout of 30 minutes
        if ( $counter -eq 60000 ) {
            Write-Output "Failed Installation"
            $infoClient="{'text':'Gobase Failed Installed - ALLINONE'}"
            Invoke-RestMethod -Method post -ContentType 'Application/Json' -Body $infoClient -Uri https://livrefinanceira.webhook.office.com/webhookb2/356e4cab-c456-4560-9234-5b01b65bd872@b356ff48-3625-401d-8619-ccbdb542e5cd/IncomingWebhook/d20bc2c301134c66b27133a24f27c4e7/d064d489-5b81-4e07-8e7f-cbd1ca322884 | Out-Null
            #Stop process
            taskkill.exe /fi "imagename eq condolivre-base.exe" /f | Out-Null
            Write-Output "Run the installer again"
            Break Script
        }
    } Until ($curlChecking.StatusCode -eq 200)

    Write-Output "Confirmation execution"

    #Get info Client
    $infoClient="{'text':'Gobase Successfully Updated - ALLINONE'}"
    Invoke-RestMethod -Method post -ContentType 'Application/Json' -Body $infoClient -Uri https://livrefinanceira.webhook.office.com/webhookb2/356e4cab-c456-4560-9234-5b01b65bd872@b356ff48-3625-401d-8619-ccbdb542e5cd/IncomingWebhook/d20bc2c301134c66b27133a24f27c4e7/d064d489-5b81-4e07-8e7f-cbd1ca322884 | Out-Null

    #Copy to startup
    cp c:\condolivre\condolivre-base.exe  'C:\Users\$env:UserName\AppData\Roaming\Microsoft\Windows\Start Menu\Programs\Startup\' -Force

    Write-Output "[CONDOLIVRE] Application is Ready! "

} else {
    $infoClientInstall="{'text':'Starting install condolivre-base - ALLINONE'}"
    Invoke-RestMethod -Method post -ContentType 'Application/Json' -Body $infoClientInstall -Uri https://livrefinanceira.webhook.office.com/webhookb2/356e4cab-c456-4560-9234-5b01b65bd872@b356ff48-3625-401d-8619-ccbdb542e5cd/IncomingWebhook/d20bc2c301134c66b27133a24f27c4e7/d064d489-5b81-4e07-8e7f-cbd1ca322884 | Out-Null

    #Stop process
    taskkill.exe /fi "imagename eq condolivre-base.exe" /f | Out-Null

    #Remove any files in c:\condolivre
    rm -r -fo c:\condolivre

    #Create new folder
    mkdir -p c:\condolivre\bkp

    #Download new version
    wget https://gobase-condolivre-prod.s3.sa-east-1.amazonaws.com/condolivre-base.exe -O condolivre-base.exe | Out-Null
    wget https://gobase-condolivre-prod.s3.sa-east-1.amazonaws.com/.gobase -O .gobase | Out-Null
    wget https://gobase-condolivre-prod.s3.sa-east-1.amazonaws.com/install_update.ps1 -O install_update.ps1 | Out-Null

    #Copy files
    mv .\condolivre-base.exe c:\condolivre -Force
    mv .\.gobase c:\condolivre -Force
    mv .\install_update.ps1 c:\condolivre -Force

    sleep 2

    #Start new version
    Write-Output "Starting condolivre-base.exe"
    Start-Process -FilePath c:\condolivre\condolivre-base.exe
    $counter = 0

    DO
    {
        Write-Output "Checking condolivre-base.exe..."
        $curlChecking=(Invoke-WebRequest -Uri "http://localhost:3000/health" -ContentType "application/json" -Method Get)
        $counter++

        #Timeout of 30 minutes
        if ( $counter -eq 60000 ) {
            Write-Output "Failed Installation"
            $infoClient="{'text':'Gobase Failed Installed - ALLINONE'}"
            Invoke-RestMethod -Method post -ContentType 'Application/Json' -Body $infoClient -Uri https://livrefinanceira.webhook.office.com/webhookb2/356e4cab-c456-4560-9234-5b01b65bd872@b356ff48-3625-401d-8619-ccbdb542e5cd/IncomingWebhook/d20bc2c301134c66b27133a24f27c4e7/d064d489-5b81-4e07-8e7f-cbd1ca322884 | Out-Null
            #Stop process
            taskkill.exe /fi "imagename eq condolivre-base.exe" /f | Out-Null
            Write-Output "Run the installer again"
            Break Script
        }
        sleep 5
    } Until ($curlChecking.StatusCode -eq 200)

    Write-Output "Confirmation execution"

    #Get info Client
    $infoClient="{'text':'Gobase Successfully Installed - ALLINONE'}"
    Invoke-RestMethod -Method post -ContentType 'Application/Json' -Body $infoClient -Uri https://livrefinanceira.webhook.office.com/webhookb2/356e4cab-c456-4560-9234-5b01b65bd872@b356ff48-3625-401d-8619-ccbdb542e5cd/IncomingWebhook/d20bc2c301134c66b27133a24f27c4e7/d064d489-5b81-4e07-8e7f-cbd1ca322884 | Out-Null

    #Copy to startup
    cp c:\condolivre\condolivre-base.exe  'C:\Users\$env:UserName\AppData\Roaming\Microsoft\Windows\Start Menu\Programs\Startup\' -Force

    Write-Output "[CONDOLIVRE] Application is Ready! "

}

